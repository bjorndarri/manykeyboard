package org.nanli.util.manykeyboard;

/**
 * ManyKeyboard - Enabling MultipleKeyboard on a Single PC
 * ManyKeyboardManager.java
 * <p>
 * Copyright (c) 2013, Nan Li
 * All rights reserved.
 * <p>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 * * Neither the name of Sirikata nor the names of its contributors may
 * be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import com.codeminders.hidapi.ClassPathLibraryLoader;
import com.codeminders.hidapi.HIDDevice;
import com.codeminders.hidapi.HIDDeviceInfo;
import com.codeminders.hidapi.HIDManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public enum ManyKeyboardManager {
  INSTANCE;

  static {
    //Load javahidlib and create key mappings
    ClassPathLibraryLoader.loadNativeHIDLibrary();
  }

  private final Map<Integer, DeviceListener> deviceListeners = new ConcurrentHashMap<>();
  private volatile int nextId = 0;

  /**
   * @param filter the filter to use
   * @return all devices according to the given filter
   */
  public static Collection<HIDDeviceInfo> getDeviceInfos(final DeviceFilter filter) {
    return getDevices(filter);
  }

  /**
   * Starts listening to the given device.
   * @param deviceInfo the device info
   * @param blocking if true then blocking is enabled on the device
   * @return the device id, used when adding listeners to the device
   * @throws IOException in case of a io exception
   */
  public int addDeviceListener(final HIDDeviceInfo deviceInfo, final boolean blocking) throws IOException {
    final HIDDevice hidDevice = HIDManager.getInstance().openByPath(deviceInfo.getPath());
    if (blocking) {
      hidDevice.enableBlocking();
    }
    else {
      hidDevice.disableBlocking();
    }

    deviceListeners.put(nextId, new DeviceListener(nextId, hidDevice));
    nextId++;

    return nextId - 1;
  }

  /**
   * Stops the listener for the device with the given id
   * @param id the device id
   * @throws IOException in case of a io exception
   */
  public void removeDeviceListener(final int id) throws IOException {
    deviceListeners.remove(id).close();
  }

  /**
   * Adds a listener to the device with the given id
   * @param deviceId the device id
   * @param listener the listener
   */
  public void addManyKeyEventListener(final int deviceId, final ManyKeyEventListener listener) {
    deviceListeners.get(deviceId).addListener(listener);
  }

  /**
   * Removes the given listener from the device with the given id
   * @param deviceId the device id
   * @param listener the listener
   */
  public void removeManyKeyEventListener(final int deviceId, final ManyKeyEventListener listener) {
    deviceListeners.get(deviceId).removeListener(listener);
  }

  /**
   * Get HID devices connected to the computer
   */
  private static Collection<HIDDeviceInfo> getDevices(final DeviceFilter filter) {
    try {
      final Collection<HIDDeviceInfo> deviceInfos = new ArrayList<>();
      final HIDManager manager = HIDManager.getInstance();
      for (final HIDDeviceInfo deviceInfo : manager.listDevices()) {
        if (filter.accept(deviceInfo)) {
          deviceInfos.add(deviceInfo);
        }
      }

      return deviceInfos;
    }
    catch (final IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static final class DeviceListener {

    private static final int BUFSIZE = 2048;

    private final int id;
    private final HIDDevice device;
    private final byte[] buffer = new byte[BUFSIZE];
    private final Set<Integer> keyFlags = new HashSet<>();

    private final ExecutorService listeningService = Executors.newSingleThreadExecutor();
    private final Collection<ManyKeyEventListener> listeners = new HashSet<>();

    private volatile boolean stopped = false;

    private int controlFlag = 0;

    DeviceListener(final int id, final HIDDevice device) {
      this.id = id;
      this.device = device;
      keyFlags.add(0);
      listeningService.submit((Runnable) () -> {
        while (!stopped) {
          readFromDevice(DeviceListener.this);
        }
      });
    }

    void addListener(final ManyKeyEventListener listener) {
      synchronized (listeners) {
        listeners.add(listener);
      }
    }

    void removeListener(final ManyKeyEventListener listener) {
      synchronized (listeners) {
        listeners.remove(listener);
      }
    }

    void close() throws IOException {
      device.close();
      stopped = true;
      listeningService.shutdownNow();
    }

    int read() throws IOException {
      return device.read(buffer);
    }

    byte[] getBuffer() {
      return buffer;
    }

    int getControlFlag() {
      return controlFlag;
    }

    int getId() {
      return id;
    }

    void setControlFlag(final int controlFlag) {
      this.controlFlag = controlFlag;
    }

    Set<Integer> getKeyFlags() {
      return keyFlags;
    }

    void setKeyFlags(final Set<Integer> keyFlags) {
      this.keyFlags.clear();
      this.keyFlags.addAll(keyFlags);
    }

    void notifyListeners(final ManyKeyEvent event) {
      synchronized (listeners) {
        for (final ManyKeyEventListener listener : listeners) {
          listener.manyKeyEvent(event);
        }
      }
    }

    boolean hasListeners() {
      synchronized (listeners) {
        return listeners.size() > 0;
      }
    }
  }

  private static void readFromDevice(final DeviceListener listener) {
    try {
      //the byte array is called input report,which only works when there is an input event;
      final int bytesRead = listener.read();
      if (bytesRead != 0) {//sometimes bytesRead = 0, resulting in the periphiral keyboard always clear the hashset
        final byte[] buffer = listener.getBuffer();

        final Set<Integer> currentSet = new HashSet<>();
        int currentControl = 0;

        if (bytesRead == 8) {//mostly 8
          for (int i = 0; i < bytesRead; i++) {
            int current = buffer[i];
            if (current < 0) {
              current = current + 256;
            }

            if (i != 0) {
              currentSet.add(current);
            }
            else {//the control flag
              currentControl = current;
            }
          }
        }
        else if (bytesRead == 10) {// macbook keyboard 10
          for (int i = 0; i < bytesRead; i++) {
            if (i != 0 && i != 9) {
              int current = buffer[i];
              if (current < 0) {
                current = current + 256;
              }

              if (i != 1) {
                currentSet.add(current);
              }
              else { //the control flag
                currentControl = current;
              }
            }
          }
        }
        else {
          if (bytesRead != 0) {
            System.err.println("other than 8 or 10: " + bytesRead);
          }
        }
        //if no one listens, then return
        if (!listener.hasListeners()) {
          return;
        }

        //if not return, someone is listening, we process with the following code
        //measure the diff between current flag and last flag
        final int lastFlag = listener.getControlFlag();
        final int diff = currentControl - lastFlag;
        if (diff > 0) {// more keys are pressed
          //note that 1,2,4,8,16,32,64,128, these digits are 2^n, so we can judge which one is pressed with Binary number
          int i = 0;
          final char[] digits = Integer.toBinaryString(diff).toCharArray();
          for (final char digit : digits) {
            if (digit == '1') {
              final int power = digits.length - 1 - i;
              listener.notifyListeners(new ManyKeyEvent(listener, listener.getId(), ManyKeyEvent.KEY_PRESSED, -(int) Math.pow(2, power)));
            }
            i++;
          }
          listener.setControlFlag(currentControl);
        }
        else if (diff < 0) {// some keys are released
          int i = 0;
          final char[] digits = Integer.toBinaryString(-diff).toCharArray();
          for (final char digit : digits) {
            if (digit == '1') {
              final int power = digits.length - 1 - i;
              listener.notifyListeners(new ManyKeyEvent(listener, listener.getId(), ManyKeyEvent.KEY_RELEASED, -(int) Math.pow(2, power)));
            }
            i++;
          }
          listener.setControlFlag(currentControl);
        }

        //measure the diff between current key flags and last key flags
        final Set<Integer> lastSet = listener.getKeyFlags();
        final Set<Integer> newKeySet = subtract(currentSet, lastSet);
        final Set<Integer> removedKeySet = subtract(lastSet, currentSet);

        if (newKeySet.size() != 0) {//new key pressed
          for (final Integer key : newKeySet) {
            listener.notifyListeners(new ManyKeyEvent(listener, listener.getId(), ManyKeyEvent.KEY_PRESSED, key));
          }
        }

        if (removedKeySet.size() != 0) {//key released
          for (final Integer key : removedKeySet) {
            listener.notifyListeners(new ManyKeyEvent(listener, listener.getId(), ManyKeyEvent.KEY_RELEASED, key));
          }
        }
        listener.setKeyFlags(currentSet);
      }
    }
    catch (final IOException e) {
      System.err.println(e);
    }
  }

  private static Set subtract(final Set set1, final Set set2) {
    final Set result = new HashSet(set1);
    result.removeAll(set2);

    return result;
  }
}
