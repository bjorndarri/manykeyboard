package org.nanli.util.manykeyboard;

import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;

/**
 * ManyKeyboard - Enabling MultipleKeyboard on a Single PC
 * ManyKeyEvent.java
 * <p>
 * Copyright (c) 2013, Nan Li
 * All rights reserved.
 * <p>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 * * Neither the name of Sirikata nor the names of its contributors may
 * be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
public final class ManyKeyEvent extends EventObject {

  public static final int KEY_PRESSED = "KEY_PRESSED".hashCode();
  public static final int KEY_RELEASED = "KEY_RELEASED".hashCode();
  public static final int MK_A = 0x04;	/* a or A */
  public static final int MK_B = 0x05;	/* b or B */
  public static final int MK_C = 0x06;	/* c or C */
  public static final int MK_D = 0x07;	/* d or D */
  public static final int MK_E = 0x08;	/* e or E */
  public static final int MK_F = 0x09;	/* f or F */
  public static final int MK_G = 0x0A;	/* g or G */
  public static final int MK_H = 0x0B;	/* h or H */
  public static final int MK_I = 0x0C;	/* i or I */
  public static final int MK_J = 0x0D;	/* j or J */
  public static final int MK_K = 0x0E;	/* k or K */
  public static final int MK_L = 0x0F;	/* l or L */
  public static final int MK_M = 0x10;	/* m or M */
  public static final int MK_N = 0x11;	/* n or N */
  public static final int MK_O = 0x12;	/* o or O */
  public static final int MK_P = 0x13;	/* p or P */
  public static final int MK_Q = 0x14;	/* q or Q */
  public static final int MK_R = 0x15;	/* r or R */
  public static final int MK_S = 0x16;	/* s or S */
  public static final int MK_T = 0x17;	/* t or T */
  public static final int MK_U = 0x18;	/* u or U */
  public static final int MK_V = 0x19;	/* v or V */
  public static final int MK_W = 0x1A;	/* w or W */
  public static final int MK_X = 0x1B;	/* x or X */
  public static final int MK_Y = 0x1C;	/* y or Y */
  public static final int MK_Z = 0x1D;	/* z or Z */
  public static final int MK_1 = 0x1E;	/* 1 or ! */
  public static final int MK_2 = 0x1F;	/* 2 or @ */
  public static final int MK_3 = 0x20;	/* 3 or # */
  public static final int MK_4 = 0x21;	/* 4 or $ */
  public static final int MK_5 = 0x22;	/* 5 or % */
  public static final int MK_6 = 0x23;	/* 6 or ^ */
  public static final int MK_7 = 0x24;	/* 7 or & */
  public static final int MK_8 = 0x25;	/* 8 or * */
  public static final int MK_9 = 0x26;	/* 9 or ( */
  public static final int MK_0 = 0x27;	/* 0 or ) */
  public static final int MK_RETURN_OR_ENTER = 0x28;	/* Return (Enter) */
  public static final int MK_ESCAPE = 0x29;	/* Escape */
  public static final int MK_DELETE_OR_BACKSPACE = 0x2A;	/* Delete (Backspace) */
  public static final int MK_TAB = 0x2B;	/* Tab */
  public static final int MK_SPACEBAR = 0x2C;	/* Spacebar */
  public static final int MK_HYPHEN = 0x2D;	/* - or _ */
  public static final int MK_EQUAL_SIGN = 0x2E;	/* = or + */
  public static final int MK_OPEN_BRACKET = 0x2F;	/* [ or { */
  public static final int MK_CLOSE_BRACKET = 0x30;	/* ] or } */
  public static final int MK_BACKSLASH = 0x31;	/* \ or | */
  public static final int MK_NON_US_POUND = 0x32;	/* Non-US # or _ */
  public static final int MK_SEMICOLON = 0x33;	/* ; or : */
  public static final int MK_QUOTE = 0x34;	/* ' or " */
  public static final int MK_GRAVE_ACCENT_AND_TILDE = 0x35;	/* Grave Accent and Tilde */
  public static final int MK_COMMA = 0x36;	/* ; or < */
  public static final int MK_PERIOD = 0x37;	/* . or > */
  public static final int MK_SLASH = 0x38;	/* / or ? */
  public static final int MK_CAPS_LOCK = 0x39;	/* Caps Lock */
  public static final int MK_F1 = 0x3A;	/* F1 */
  public static final int MK_F2 = 0x3B;	/* F2 */
  public static final int MK_F3 = 0x3C;	/* F3 */
  public static final int MK_F4 = 0x3D;	/* F4 */
  public static final int MK_F5 = 0x3E;	/* F5 */
  public static final int MK_F6 = 0x3F;	/* F6 */
  public static final int MK_F7 = 0x40;	/* F7 */
  public static final int MK_F8 = 0x41;	/* F8 */
  public static final int MK_F9 = 0x42;	/* F9 */
  public static final int MK_F10 = 0x43;	/* F10 */
  public static final int MK_F11 = 0x44;	/* F11 */
  public static final int MK_F12 = 0x45;	/* F12 */
  public static final int MK_PRINT_SCREEN = 0x46;	/* Print Screen */
  public static final int MK_SCROLL_LOCK = 0x47;	/* Scroll Lock */
  public static final int MK_PAUSE = 0x48;	/* Pause */
  public static final int MK_INSERT = 0x49;	/* Insert */
  public static final int MK_HOME = 0x4A;	/* Home */
  public static final int MK_PAGE_UP = 0x4B;	/* Page Up */
  public static final int MK_DELETE_FORWARD = 0x4C;	/* Delete Forward */
  public static final int MK_END = 0x4D;	/* End */
  public static final int MK_PAGE_DOWN = 0x4E;	/* Page Down */
  public static final int MK_RIGHT_ARROW = 0x4F;	/* Right Arrow */
  public static final int MK_LEFT_ARROW = 0x50;	/* Left Arrow */
  public static final int MK_DOWN_ARROW = 0x51;	/* Down Arrow */
  public static final int MK_UP_ARROW = 0x52;	/* Up Arrow */
  public static final int MK_PAD_NUM_LOCK = 0x53;	/* Keypad NumLock or Clear */
  public static final int MK_PAD_SLASH = 0x54;	/* Keypad / */
  public static final int MK_PAD_ASTERISK = 0x55;	/* Keypad * */
  public static final int MK_PAD_HYPHEN = 0x56;	/* Keypad - */
  public static final int MK_PAD_PLUS = 0x57;	/* Keypad + */
  public static final int MK_PAD_ENTER = 0x58;	/* Keypad Enter */
  public static final int MK_PAD_1 = 0x59;	/* Keypad 1 or End */
  public static final int MK_PAD_2 = 0x5A;	/* Keypad 2 or Down Arrow */
  public static final int MK_PAD_3 = 0x5B;	/* Keypad 3 or Page Down */
  public static final int MK_PAD_4 = 0x5C;	/* Keypad 4 or Left Arrow */
  public static final int MK_PAD_5 = 0x5D;	/* Keypad 5 */
  public static final int MK_PAD_6 = 0x5E;	/* Keypad 6 or Right Arrow */
  public static final int MK_PAD_7 = 0x5F;	/* Keypad 7 or Home */
  public static final int MK_PAD_8 = 0x60;	/* Keypad 8 or Up Arrow */
  public static final int MK_PAD_9 = 0x61;	/* Keypad 9 or Page Up */
  public static final int MK_PAD_0 = 0x62;	/* Keypad 0 or Insert */
  public static final int MK_PAD_PERIOD = 0x63;	/* Keypad . or Delete */
  public static final int MK_NON_US_BACKSLASH = 0x64;	/* Non-US \ or | */
  public static final int MK_APPLICATION = 0x65;	/* Application */
  public static final int MK_POWER = 0x66;	/* Power */
  public static final int MK_PAD_EQUAL_SIGN = 0x67;	/* Keypad = */
  public static final int MK_F13 = 0x68;	/* F13 */
  public static final int MK_F14 = 0x69;	/* F14 */
  public static final int MK_F15 = 0x6A;	/* F15 */
  public static final int MK_F16 = 0x6B;	/* F16 */
  public static final int MK_F17 = 0x6C;	/* F17 */
  public static final int MK_F18 = 0x6D;	/* F18 */
  public static final int MK_F19 = 0x6E;	/* F19 */
  public static final int MK_F20 = 0x6F;	/* F20 */
  public static final int MK_F21 = 0x70;	/* F21 */
  public static final int MK_F22 = 0x71;	/* F22 */
  public static final int MK_F23 = 0x72;	/* F23 */
  public static final int MK_F24 = 0x73;	/* F24 */
  public static final int MK_EXECUTE = 0x74;	/* Execute */
  public static final int MK_HELP = 0x75;	/* Help */
  public static final int MK_MENU = 0x76;	/* Menu */
  public static final int MK_SELECT = 0x77;	/* Select */
  public static final int MK_STOP = 0x78;	/* Stop */
  public static final int MK_AGAIN = 0x79;	/* Again */
  public static final int MK_UNDO = 0x7A;	/* Undo */
  public static final int MK_CUT = 0x7B;	/* Cut */
  public static final int MK_COPY = 0x7C;	/* Copy */
  public static final int MK_PASTE = 0x7D;	/* Paste */
  public static final int MK_FIND = 0x7E;	/* Find */
  public static final int MK_MUTE = 0x7F;	/* Mute */
  public static final int MK_VOLUME_UP = 0x80;	/* Volume Up */
  public static final int MK_VOLUME_DOWN = 0x81;	/* Volume Down */
  public static final int MK_LOCKING_CAPS_LOCK = 0x82;	/* Locking Caps Lock */
  public static final int MK_LOCKING_NUM_LOCK = 0x83;	/* Locking Num Lock */
  public static final int MK_LOCKING_SCROLL_LOCK = 0x84;	/* Locking Scroll Lock */
  public static final int MK_PAD_COMMA = 0x85;	/* Keypad Comma */
  public static final int MK_PAD_EQUAL_SIGN_AS_400 = 0x86;	/* Keypad Equal Sign for AS/400 */
  public static final int MK_INTERNATIONAL_1 = 0x87;	/* International1 */
  public static final int MK_INTERNATIONAL_2 = 0x88;	/* International2 */
  public static final int MK_INTERNATIONAL_3 = 0x89;	/* International3 */
  public static final int MK_INTERNATIONAL_4 = 0x8A;	/* International4 */
  public static final int MK_INTERNATIONAL_5 = 0x8B;	/* International5 */
  public static final int MK_INTERNATIONAL_6 = 0x8C;	/* International6 */
  public static final int MK_INTERNATIONAL_7 = 0x8D;	/* International7 */
  public static final int MK_INTERNATIONAL_8 = 0x8E;	/* International8 */
  public static final int MK_INTERNATIONAL_9 = 0x8F;	/* International9 */
  public static final int MK_LANG1 = 0x90;	/* LANG1 */
  public static final int MK_LANG2 = 0x91;	/* LANG2 */
  public static final int MK_LANG3 = 0x92;	/* LANG3 */
  public static final int MK_LANG4 = 0x93;	/* LANG4 */
  public static final int MK_LANG5 = 0x94;	/* LANG5 */
  public static final int MK_LANG6 = 0x95;	/* LANG6 */
  public static final int MK_LANG7 = 0x96;	/* LANG7 */
  public static final int MK_LANG8 = 0x97;	/* LANG8 */
  public static final int MK_LANG9 = 0x98;	/* LANG9 */
  public static final int MK_ALTERNATE_ERASE = 0x99;	/* AlternateErase */
  public static final int MK_SYS_REQ_OR_ATTENTION = 0x9A;	/* SysReq/Attention */
  public static final int MK_CANCEL = 0x9B;	/* Cancel */
  public static final int MK_CLEAR = 0x9C;	/* Clear */
  public static final int MK_PRIOR = 0x9D;	/* Prior */
  public static final int MK_RETURN = 0x9E;	/* Return */
  public static final int MK_SEPARATOR = 0x9F;	/* Separator */
  public static final int MK_OUT = 0xA0;	/* Out */
  public static final int MK_OPER = 0xA1;	/* Oper */
  public static final int MK_CLEAR_OR_AGAIN = 0xA2;	/* Clear/Again */
  public static final int MK_CR_SEL_OR_PROPS = 0xA3;	/* CrSel/Props */
  public static final int MK_EX_SEL = 0xA4;	/* ExSel */

  //Modifier key
  public static final int MK_LEFT_CONTROL = -1;	/* Left Control */
  public static final int MK_LEFT_SHIFT = -2;	/* Left Shift */
  public static final int MK_LEFT_ALT = -4;	/* Left Alt */
  public static final int MK_LEFT_GUI = -8;	/* Left GUI */
  public static final int MK_RIGHT_CONTROL = -16;	/* Right Control */
  public static final int MK_RIGHT_SHIFT = -32;	/* Right Shift */
  public static final int MK_RIGHT_ALT = -64;	/* Right Alt */
  public static final int MK_RIGHT_GUI = -128;	/* Right GUI */

  private static final Map<Integer, String> KEY_MAP = new HashMap<Integer, String>();
  private static final Map<Integer, String> MODIFIED_KEY_MAP = new HashMap<Integer, String>();

  static {
    KEY_MAP.put(ManyKeyEvent.MK_A, "a");
    KEY_MAP.put(ManyKeyEvent.MK_B, "b");
    KEY_MAP.put(ManyKeyEvent.MK_C, "c");
    KEY_MAP.put(ManyKeyEvent.MK_D, "d");
    KEY_MAP.put(ManyKeyEvent.MK_E, "e");
    KEY_MAP.put(ManyKeyEvent.MK_F, "f");
    KEY_MAP.put(ManyKeyEvent.MK_G, "g");
    KEY_MAP.put(ManyKeyEvent.MK_H, "h");
    KEY_MAP.put(ManyKeyEvent.MK_I, "i");
    KEY_MAP.put(ManyKeyEvent.MK_J, "j");
    KEY_MAP.put(ManyKeyEvent.MK_K, "k");
    KEY_MAP.put(ManyKeyEvent.MK_L, "l");
    KEY_MAP.put(ManyKeyEvent.MK_M, "m");
    KEY_MAP.put(ManyKeyEvent.MK_N, "n");
    KEY_MAP.put(ManyKeyEvent.MK_O, "o");
    KEY_MAP.put(ManyKeyEvent.MK_P, "p");
    KEY_MAP.put(ManyKeyEvent.MK_Q, "q");
    KEY_MAP.put(ManyKeyEvent.MK_R, "r");
    KEY_MAP.put(ManyKeyEvent.MK_S, "s");
    KEY_MAP.put(ManyKeyEvent.MK_T, "t");
    KEY_MAP.put(ManyKeyEvent.MK_U, "u");
    KEY_MAP.put(ManyKeyEvent.MK_V, "v");
    KEY_MAP.put(ManyKeyEvent.MK_W, "w");
    KEY_MAP.put(ManyKeyEvent.MK_X, "x");
    KEY_MAP.put(ManyKeyEvent.MK_Y, "y");
    KEY_MAP.put(ManyKeyEvent.MK_Z, "z");
    KEY_MAP.put(ManyKeyEvent.MK_1, "1");
    KEY_MAP.put(ManyKeyEvent.MK_2, "2");
    KEY_MAP.put(ManyKeyEvent.MK_3, "3");
    KEY_MAP.put(ManyKeyEvent.MK_4, "4");
    KEY_MAP.put(ManyKeyEvent.MK_5, "5");
    KEY_MAP.put(ManyKeyEvent.MK_6, "6");
    KEY_MAP.put(ManyKeyEvent.MK_7, "7");
    KEY_MAP.put(ManyKeyEvent.MK_8, "8");
    KEY_MAP.put(ManyKeyEvent.MK_9, "9");
    KEY_MAP.put(ManyKeyEvent.MK_0, "0");
    KEY_MAP.put(ManyKeyEvent.MK_SPACEBAR, " ");
    KEY_MAP.put(ManyKeyEvent.MK_HYPHEN, "-");
    KEY_MAP.put(ManyKeyEvent.MK_EQUAL_SIGN, "=");
    KEY_MAP.put(ManyKeyEvent.MK_OPEN_BRACKET, "[");
    KEY_MAP.put(ManyKeyEvent.MK_CLOSE_BRACKET, "]");
    KEY_MAP.put(ManyKeyEvent.MK_BACKSLASH, "\\");
    KEY_MAP.put(ManyKeyEvent.MK_SEMICOLON, ";");
    KEY_MAP.put(ManyKeyEvent.MK_QUOTE, "\'");
    KEY_MAP.put(ManyKeyEvent.MK_GRAVE_ACCENT_AND_TILDE, "`");
    KEY_MAP.put(ManyKeyEvent.MK_COMMA, ",");
    KEY_MAP.put(ManyKeyEvent.MK_PERIOD, ".");
    KEY_MAP.put(ManyKeyEvent.MK_SLASH, "/");

    KEY_MAP.put(ManyKeyEvent.MK_PAD_SLASH, "/");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_ASTERISK, "*");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_HYPHEN, "-");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_PLUS, "+");

    KEY_MAP.put(ManyKeyEvent.MK_PAD_1, "1");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_2, "2");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_3, "3");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_4, "4");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_5, "5");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_6, "6");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_7, "7");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_8, "8");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_9, "9");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_0, "0");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_PERIOD, ".");
    KEY_MAP.put(ManyKeyEvent.MK_PAD_COMMA, ",");

    KEY_MAP.put(ManyKeyEvent.MK_LEFT_CONTROL, "L_CONTROL");
    KEY_MAP.put(ManyKeyEvent.MK_LEFT_SHIFT, "L_SHIFT");
    KEY_MAP.put(ManyKeyEvent.MK_LEFT_ALT, "L_ALT");
    KEY_MAP.put(ManyKeyEvent.MK_LEFT_GUI, "L_GUI");
    KEY_MAP.put(ManyKeyEvent.MK_RIGHT_CONTROL, "R_CONTROL");
    KEY_MAP.put(ManyKeyEvent.MK_RIGHT_SHIFT, "R_SHIFT");
    KEY_MAP.put(ManyKeyEvent.MK_RIGHT_ALT, "R_ALT");
    KEY_MAP.put(ManyKeyEvent.MK_RIGHT_GUI, "R_GUI");

    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_A, "A");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_B, "B");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_C, "C");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_D, "D");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_E, "E");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_F, "F");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_G, "G");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_H, "H");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_I, "I");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_J, "J");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_K, "K");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_L, "L");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_M, "M");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_N, "N");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_O, "O");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_P, "P");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_Q, "Q");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_R, "R");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_S, "S");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_T, "T");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_U, "U");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_V, "V");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_W, "W");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_X, "X");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_Y, "Y");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_Z, "Z");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_1, "!");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_2, "@");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_3, "#");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_4, "$");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_5, "%");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_6, "^");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_7, "&");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_8, "*");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_9, "(");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_0, ")");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_HYPHEN, "_");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_EQUAL_SIGN, "+");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_OPEN_BRACKET, "{");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_CLOSE_BRACKET, "}");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_BACKSLASH, "|");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_SEMICOLON, ":");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_QUOTE, "\"");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_GRAVE_ACCENT_AND_TILDE, "~");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_COMMA, "<");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_PERIOD, ">");
    MODIFIED_KEY_MAP.put(ManyKeyEvent.MK_SLASH, "?");
  }

  private final int keyCode;
  private final int type;
  private final int deviceId;

  /**
   * Constructs a prototypical Event.
   * @param source The object on which the Event initially occurred.
   * @throws IllegalArgumentException if source is null.
   */
  public ManyKeyEvent(final Object source, final int deviceId, final int type, final int keyCode) {
    super(source);
    this.deviceId = deviceId;
    this.keyCode = keyCode;
    this.type = type;
  }

  public boolean isActionKey() {
    return !KEY_MAP.containsKey(keyCode);
  }

  public int getDeviceId() {
    return deviceId;
  }

  public boolean isModifierKey() {
    return keyCode < 0;
  }

  public int getKeyCode() {
    return keyCode;
  }

  public String getKeystring() {
    return KEY_MAP.get(keyCode);
  }

  public boolean isKeyPressed() {
    return type == KEY_PRESSED;
  }

  public boolean isKeyReleased() {
    return type == KEY_RELEASED;
  }

  public String getKeyType() {
    if (type == KEY_PRESSED) {
      return "KEY_PRESSED";
    }
    else {
      return "KEY_RELEASED";
    }
  }

  @Override
  public String toString() {
    return "Id: " + deviceId + ", " + getKeyType() + ", " + KEY_MAP.get(keyCode) + (", keyCode: " + Integer.toHexString(keyCode));
  }
}
